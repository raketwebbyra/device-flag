# Device Detect

Adds flag to your url depending on which device you use

    //Include Device Flagg Class to your index.php file
    require('deviceflag/deviceflag.php');

    $flag = new Device_Flag($flag = 'device', $mobile = true, $tablet = true, $mobile_param = 'm', $tablet_param = 't');